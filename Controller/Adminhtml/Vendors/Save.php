<?php
namespace Elogic\Vendors\Controller\Adminhtml\Vendors;

use Elogic\Vendors\Model\VendorsRepository;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use RuntimeException;

class Save extends Action
{
    /**
     * @var
     */
    private $vendorsRepository;

    /**
         * @param Action\Context $context
         * @param \Elogic\Vendors\Model\Vendors $model
         */
    public function __construct(
        Action\Context $context,
        VendorsRepository $vendorsRepository
    ) {
        $this->vendorsRepository = $vendorsRepository;
        parent::__construct($context);
    }

    /**
         * {@inheritdoc}
         */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Elogic_Vendors::vendors_save');
    }

    /**
         * Save action
         *
         * @return \Magento\Framework\Controller\ResultInterface
         */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
            $model = $this->vendorsRepository->get($id);
            $model->setData($data);
            try {
                $this->vendorsRepository->save($model);
                $this->messageManager->addSuccessMessage(__('Vendor saved'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving vendor'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
