<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 2019-02-14
 * Time: 09:33
 */

namespace Elogic\Vendors\Api\Data;


use Magento\Framework\Api\ExtensibleDataInterface;

interface VendorsInterface extends ExtensibleDataInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID = 'entity_id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const DATE_ADDED = 'date_added';
    const LOGO = 'logo';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int\null
     */
    public function getID();

    /**
     * Set ID
     *
     * @param int $entity_id
     * @return $this
     */
    public function setId($entity_id);

    /**
     * Get Name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get Description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Set Description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * Get Date Added
     *
     * @return $this
     */
    public function getDateAdded();

    /**
     * Set Date Added
     *
     * @param string $dateAdded
     * @return string
     */
    public function setDateAdded($dateAdded);
}