<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.02.2019
 * Time: 11:44
 */

namespace Elogic\Vendors\Api\Data;


use Magento\Framework\Api\SearchResultsInterface;

interface VendorsSearchResultInterface extends SearchResultsInterface
{

    public function getItems();

    public function setItems(array $items);
}