<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 2019-02-14
 * Time: 09:32
 */

namespace Elogic\Vendors\Api;

use Elogic\Vendors\Api\Data\VendorsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface VendorsRepositoryInterface
{
    /**
     * Create or update vendors.
     *
     * @param VendorsInterface $vendors
     * @return mixed
     */
    public function save(VendorsInterface $vendors);

    /**
     * Retrieve vendors.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Get vendors by entity id.
     *
     * @param $entity_id
     * @return mixed
     */
    public function get($entity_id);

    /**
     * Delete vendors.
     *
     * @param VendorsInterface $vendors
     * @return mixed
     */
    public function delete(VendorsInterface $vendors);

    /**
     * Delete vendors by entity id.
     *
     * @param $entity_id
     * @return mixed
     */
    public function deleteById($entity_id);
}