<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 2019-01-29
 * Time: 09:03
 */

namespace Elogic\Vendors\Model;


use Elogic\Vendors\Api\Data\VendorsInterface;
use Magento\Framework\Model\AbstractModel;

class Vendors extends AbstractModel implements VendorsInterface
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'vendors'; // parent value is 'core_abstract'

    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'vendor'; // parent value is 'object'

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = VendorsInterface::ENTITY_ID;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Elogic\Vendors\Model\Resource\Vendors');
    }

    public function getId()
    {
        return $this->getData(VendorsInterface::ENTITY_ID);
    }

    public function setId($entity_id)
    {
        $this->setData(VendorsInterface::ENTITY_ID, $entity_id);
        return $this;
    }

    public function getName()
    {
        return $this->getData(VendorsInterface::NAME);
    }

    public function setName($name)
    {
        $this->setData(VendorsInterface::NAME, $name);
        return $this;
    }

    public function getDescription()
    {
        return $this->getData(VendorsInterface::DESCRIPTION);
    }

    public function setDescription($description)
    {
        $this->setData(VendorsInterface::DESCRIPTION, $description);
        return $this;
    }

    public function getDateAdded()
    {
        return $this->getData(VendorsInterface::DATE_ADDED);
    }

    public function setDateAdded($dateAdded)
    {
        $this->setData(VendorsInterface::DATE_ADDED, $dateAdded);
    }
}