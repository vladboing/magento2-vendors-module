<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 2019-01-29
 * Time: 09:09
 */

namespace Elogic\Vendors\Model\Resource;


use Elogic\Vendors\Api\Data\VendorsInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Vendors extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('elogic_vendors', VendorsInterface::ENTITY_ID);
    }
}