<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 2019-01-29
 * Time: 09:57
 */

namespace Elogic\Vendors\Model\Resource\Vendors;

use Elogic\Vendors\Model\Vendors;
use Elogic\Vendors\Model\Resource\Vendors as VendorsResource;
use Elogic\Vendors\Api\Data\VendorsInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Vendors::class, VendorsResource::class);
    }

    protected $_idFieldName = VendorsInterface::ENTITY_ID;
}