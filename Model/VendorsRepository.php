<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.02.2019
 * Time: 10:57
 */

namespace Elogic\Vendors\Model;


use Elogic\Vendors\Api\Data\VendorsInterface;
use Elogic\Vendors\Api\VendorsRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Elogic\Vendors\Model\Resource\Vendors as VendorsResource;

class VendorsRepository implements VendorsRepositoryInterface
{
    private $registry = [];
    private $vendorsResource;
    private $vendorsFactory;
    private $vendorsCollectionFactory;
    private $vendorsSearchResultFactory;

    public function __construct(
        VendorsResource $vendorsResource,
        VendorsFactory $vendorsFactory,
        Resource\Vendors\CollectionFactory $vendorsCollectionFactory,
        VendorsSearchResultFactory $vendorsSearchResultFactory
    )
    {
        $this->vendorsResource = $vendorsResource;
        $this->vendorsFactory = $vendorsFactory;
        $this->vendorsCollectionFactory = $vendorsCollectionFactory;
        $this->vendorsSearchResultFactory = $vendorsSearchResultFactory;
    }

    public function get($entity_id)
    {
        if (!array_key_exists($entity_id, $this->registry)) {
            $vendors = $this->vendorsFactory->create();
            $this->vendorsResource->load($vendors, $entity_id);
            $this->registry[$entity_id] = $vendors;
        }

        return $this->registry[$entity_id];
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->vendorsCollectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $searchResult = $this->vendorsSearchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }

    public function save(VendorsInterface $vendors)
    {
        try {
            $this->vendorsResource->save($vendors);
            $this->registry[$vendors->getID()] = $this->get($vendors->getID());
        } catch (\Exception $exception) {
            throw new StateException(__('Unable to save vendor #%1', $vendors->getID()));
        }
        return $this->registry[$vendors->getID()];
    }

    public function delete(VendorsInterface $vendors)
    {
        try {
            $this->vendorsResource->delete($vendors);
            unset($this->registry[$vendors->getID()]);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove vendor #%1', $vendors->getID()));
        }

        return true();
    }

    public function deleteById($entity_id)
    {
        return $this->delete($this->get($entity_id));
    }
}