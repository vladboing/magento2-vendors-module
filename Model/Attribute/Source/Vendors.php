<?php

namespace Elogic\Vendors\Model\Attribute\Source;

use Elogic\Vendors\Model\Resource\Vendors\Collection;
use Elogic\Vendors\Model\Resource\Vendors\CollectionFactory;


class Vendors extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    protected $collectionFactory;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            /** @var Collection $collection */
            $collection = $this->collectionFactory->create();

            $vendors = $collection->getItems();

            if (!$vendors) {
                return [];
            }

            $options = [];

            foreach ($vendors as $vendor) {
                $options[] = [
                    'label' => $vendor->getData('name'),
                    'value' => $vendor->getData('name'),
                ];
            }

            $this->_options = $options;
        }

        return $this->_options;
    }
}