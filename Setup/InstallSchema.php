<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 28.01.2019
 * Time: 22:22
 */

namespace Elogic\Vendors\Setup;


use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Check if the table already exists
         */
        if(!$installer->tableExists('elogic_vendors')) {
            /**
             * Create 'elogic_vendors' table
             */
            $tableName = $installer->getTable('elogic_vendors');
            $tableComment = 'Vendors list for vendors module';
            $table = $setup->getConnection()
                ->newTable($tableName)
                ->addColumn(
                'entity_id',
                Table::TYPE_INTEGER,
                null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                'Vendor Id'
                )
                ->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false,
                    'default' => ''
                ],
                'Vendor Name'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    2048,
                    [
                        'nullable' => false,
                        'default' => ''
                    ],
                    'Vendor Description'
                )
                ->addColumn(
                    'date_added',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => Table::TIMESTAMP_INIT
                    ],
                    'Date of adding'
                )
                ->addColumn(
                    'logo',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false,
                        'default' => ''
                    ],
                    'Vendor Logo'
                )
                ->setComment($tableComment);
            $installer->getConnection()->createTable($table);
        }

        // End setup
        $installer->endSetup();
    }
}